
## Introduzione

Partendo da diversi documenti pubblicati in questi anni sul tema CyberSecurity nella PA, ovvero:

* [Misure Minime di Sicurezza ICT](https://www.agid.gov.it/it/sicurezza/misure-minime-sicurezza-ict)
* [Piano triennale per la PA](https://pianotriennale-ict.italia.it/)

e considerando le SANS 20

* [Sans 20](https://www.sans.org/course/implementing-auditing-critical-security-controls)

anche alla luce dell’avvento del:

* [GDPR](https://www.garanteprivacy.it/regolamentoue)

che ha migliorato nettamente la comprensione della privacy all’interno dei piccoli comuni (laddove implementato perlomeno un registro dei trattamenti e laddove fatta un’analisi del rischio) si è cercato di scrivere una guida in 10 punti, quindi breve ma chiara sia per addetti ai lavori che non, per migliorare la sicurezza informatica negli enti di piccole dimensioni (PAL).

Questo allo scopo di unire i principi di privacy e security, due facce della stessa medaglia, e dare delle linee pratiche di azione.

## CyberPareto

Il [principio di Pareto](https://it.wikipedia.org/wiki/Principio_di_Pareto) dice che “la maggior parte degli effetti è dovuta a un numero ristretto di cause”.
L’obiettivo del documento è quindi di individuare il minor numero possibile di cause che generano il maggior numero di problemi di CyberSecurity nelle PAL.

## Standard di Sicurezza Minima: Umani

Un essere umano è il pericolo numero uno per chi fa CyberSicurezza.<br>
Legenda: BR : basso rischio | MR: medio rischio | AR: alto rischio | TR = task ricorrente

| Standards                    | TR | Cosa fare                                         | BR | MR | AR |
| -----------------------------|----|---------------------------------------------------|----|----|----|
| Formazione Sicurezza         |![rotate](./images/rotate.png)|Formare ogni anno per 2 ore il personale sui rischi della cybersicurezza, <br> derivante dalla poca consapevolezza del funzionamento degli attacchi<br> informatici. <br><br>In particolare formare sui 3 vettori di attacco principali indicati dal Rapporto Clusit dell’anno precedente.<br><br> Alla data di scrittura:<br> * phishing<br>* spear phishing<br>* social engineering<br><br>Sottolineare l’importanza della gestione password con un password manager. |![green](./images/green.png)|![orange](./images/orange.png)|![red](./images/red.png)|
| Formazione Privacy           |![rotate](./images/rotate.png)|Formare ogni anno per 2 ore il personale sui rischi derivanti dalla poca <br> conoscenza del valore dei dati. |![green](./images/green.png)|![orange](./images/orange.png)|![red](./images/red.png)|


